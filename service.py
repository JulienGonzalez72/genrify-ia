from keras.models import load_model
import numpy as np
import json
from os import remove, path

model = load_model('./model')

genres = ['blues', 'classical', 'country', 'disco',
          'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']


def normalize_mfcc(mfcc):
    mfcc = np.resize(mfcc, (20, 6100))
    return np.ndarray.flatten(mfcc)


def predict(mfcc):
    mfcc = normalize_mfcc(mfcc)
    x = np.reshape(mfcc, (1, len(mfcc)))
    prediction = model.predict(x)[0]
    return prediction


def train(mfcc, genre):
    mfcc = normalize_mfcc(mfcc)
    x = np.reshape(mfcc, (1, len(mfcc)))
    categorical_array = np.full(len(genres), 0)
    categorical_array[genres.index(genre)] = 1
    y = np.reshape(categorical_array, (1, len(genres)))
    model.fit(x, y, epochs=10)
    model.save('./model')


def train_corrections():
    if path.exists('./corrections.csv'):
        x = []
        y = []
        readable_file = open('./corrections.csv', 'r')
        for line in readable_file.readlines():
            array = line.split(',', 1)
            genre = array[0]
            mfcc = np.array(json.loads(array[1]))
            print(len(mfcc))
            mfcc = normalize_mfcc(mfcc)
            x.append(mfcc)
            categorical_array = np.full(len(genres), 0)
            categorical_array[genres.index(genre)] = 1
            y.append(categorical_array)
        readable_file.close()
        remove('./corrections.csv')
        x = np.array(x)
        y = np.array(y)
        model.fit(x, y, epochs=10)
        model.save('./model')


def store_correction(mfcc, genre):
    line = str(genre) + ',' + str(mfcc) + '\n'
    with open('./corrections.csv', 'a') as corrections_file:
        corrections_file.write(line)
        corrections_file.flush()
