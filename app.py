import firebase_admin
import pyrebase
import json
from flask import Flask, request, jsonify, make_response
from firebase_admin import credentials, auth
from functools import wraps
import service
import numpy as np

app = Flask('Genrify')

users = [{'uid': 1, 'name': 'Noah Schairer'}]

# Connect to firebase
cred = credentials.Certificate('firebaseAdminGenrify.json')
firebase = firebase_admin.initialize_app(cred)
pb = pyrebase.initialize_app(json.load(open('firebaseConfigGenrify.json')))


def check_token(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if not request.headers.get('authorization'):
            return {'message': 'No token provided'}, 400
        try:
            user = auth.verify_id_token(request.headers['authorization'])
            request.user = user
        except:
            return {'message': 'Invalid token provided.'}, 400
        return f(*args, **kwargs)
    return wrap


# Api route to get users
@app.route('/userinfo', methods=['POST'])
@check_token
def userinfo():
    return {'data': request.user}, 200


# Api route to sign up a new user
@app.route('/signup')
def signup():
    email = request.form.get('email')
    password = request.form.get('mdp')
    print(email)
    print(password)
    if email is None or password is None:
        return {'message': 'Error missing email or password'}, 400
    try:
        user = auth.create_user(
            email=email,
            password=password
        )
        return {'message': f'Successfully created user {user.uid}'}, 200
    except:
        return {'message': 'Error creating user'}, 400


# Api route to get a new token for a valid user
@app.route('/token')
def token():
    email = request.form.get('email')
    password = request.form.get('mdp')
    try:
        user = pb.auth().sign_in_with_email_and_password(email, password)
        jwt = user['idToken']
        return {'token': jwt}, 200
    except:
        return {'message': 'There was an error logging in'}, 400


@app.route('/genre', methods=['POST'])
def get_genre():
    mfcc = request.json['mfcc']
    print(type(mfcc))
    prediction = service.predict(mfcc)
    res = []
    # Find first genre probability and index
    first_genre_index = prediction.argmax()
    first_genre = service.genres[first_genre_index]
    first_genre_prob = str(prediction[first_genre_index])
    res.append({'genre': first_genre, 'prob': first_genre_prob})
    print(prediction[first_genre_index])
    prediction = np.delete(prediction, first_genre_index)
    # Find second genre probability and index
    second_genre_index = prediction.argmax()
    second_genre = service.genres[second_genre_index]
    print(prediction[second_genre_index])
    second_genre_prob = str(prediction[second_genre_index])
    res.append({'genre': second_genre, 'prob': second_genre_prob})
    return jsonify(res)


@app.route('/rectify', methods=['POST'])
@check_token
def rectify_genre():
    mfcc = request.json['mfcc']
    new_genre = request.json['genre']
    service.store_correction(mfcc, new_genre)
    return make_response()


@app.route('/train_rectify')
def train():
    service.train_corrections()
    return make_response()
