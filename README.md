# Genrify AI

## Prerequisites

You need a folder `data` at root level with all songs to train with (a subfolder by genre). However if you want an already trained model you need to ask the owner for the `model` folder and skip to part 2.

## 1. Create and train model

```
python model.py
```

A folder `model` that is the trained model will be created.

## 2. Install dependencies (using PIP)

```
pip install -r requirements.txt
```

## 3. Run the server locally

Development :

```
cd /usr/share/genrify-ia
flask run -h localhost -p 9001 --reload --debugger
```

Production :

```
flask run
```

## 4. Call routes

### POST recette.teamber.fr/genre

**Description :** Get the genre of a musical sample with some MFCCs.<br>
**Body params :** mfcc (25800-size number array)<br>
**Response :** 2 highest probability predicted genres<br>
**Response example :**

```
[
    {
        "genre": "disco",
        "prob": "0.16395564"
    },
    {
        "genre": "metal",
        "prob": "0.120307356"
    }
]
```

### POST recette.teamber.fr/rectify

**Description :** Rectify a known genre of a musical sample with some MFCCs by storing it into a file. Genre must also be already known.<br>
**Body params :** mfcc (25800-size number array), genre (string)<br>
**Response :** OK

### POST recette.teamber.fr/train_rectify

**Description :** Train AI on every stored corrections.<br>
**Response :** OK
