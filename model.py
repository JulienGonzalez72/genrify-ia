import librosa
import librosa.feature
import librosa.display
import glob
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv2D, Flatten, MaxPool2D, SimpleRNN, Dropout
from keras.utils.np_utils import to_categorical
from time import time_ns
from service import normalize_mfcc
import json


def extract_features_song(f, genre):
    y, _ = librosa.load(f, duration=30)

    # get Mel-frequency sceptral coefficients
    mfcc = np.array(librosa.feature.mfcc(y, n_mfcc=20))
    return normalize_mfcc(mfcc)


genres = ['blues', 'classical', 'country', 'disco',
          'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
# genres = ['classical', 'rock']


def generate_features_and_labels_for_genre(genre):
    features = []
    labels = []
    sound_files = glob.glob('data/' + genre + '/*.txt')
    print('Processing %d songs in %s genre...' % (len(sound_files), genre))
    categorical_array = np.full(len(genres), 0)
    categorical_array[genres.index(genre)] = 1
    for f in sound_files:
        f2 = open(f, 'r')
        content = f2.read()
        json_content = json.loads(content)
        array = np.array(json_content)
        array = normalize_mfcc(array)
        features.append(array)
        labels.append(categorical_array)
    return features, labels


def generate_features_and_labels():
    train_features = []
    train_labels = []
    test_features = []
    test_labels = []

    t = time_ns()
    for genre in genres:
        genre_features, genre_labels = generate_features_and_labels_for_genre(
            genre)
        train_p = int(len(genre_features) * 0.8)
        train_features.extend(genre_features[:train_p])
        train_labels.extend(genre_labels[:train_p])
        test_features.extend(genre_features[train_p:])
        test_labels.extend(genre_labels[train_p:])
    print('Features extracted in %d ms' % ((time_ns() - t) / 1e6))

    train_features = np.asarray(train_features)
    train_labels = np.asarray(train_labels)
    test_features = np.asarray(test_features)
    test_labels = np.asarray(test_labels)
    return train_features, train_labels, test_features, test_labels


train_features, train_labels, test_features, test_labels = generate_features_and_labels()

print('train_features shape :', np.shape(train_features))
print('train_labels shape :', np.shape(train_labels))
print('test_features shape :', np.shape(test_features))
print('test_labels shape :', np.shape(test_labels))

# Shuffle arrays
train_shuffler = np.random.permutation(len(train_features))
train_features = train_features[train_shuffler]
train_labels = train_labels[train_shuffler]

test_shuffler = np.random.permutation(len(test_features))
test_features = test_features[test_shuffler]
test_labels = test_labels[test_shuffler]

# Test labels distribution
train_results = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for array in train_labels:
    train_results[array.tolist().index(1)] += 1
print('train_results :', train_results)

test_results = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for array in test_labels:
    test_results[array.tolist().index(1)] += 1
print('test_results :', test_results)

model = Sequential([
    Dense(200, input_dim=np.shape(train_features)[1], activation='relu'),
    Dropout(0.15),
    Dense(200, input_dim=np.shape(train_features)[1], activation='relu'),
    Dropout(0.15),
    Dense(len(genres), activation='softmax')
])

model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
print(model.summary())

history = model.fit(train_features, train_labels, epochs=30, batch_size=32,
                    validation_split=0.15, shuffle=True)
plt.plot(history.history['loss'], color='red', label='Train loss')
plt.plot(history.history['accuracy'], color='blue', label='Train accuracy')
plt.plot(history.history['val_loss'], color='green', label='Validation loss')
plt.plot(history.history['val_accuracy'],
         color='yellow', label='Validation accuracy')
plt.legend()
plt.show()

loss, acc = model.evaluate(test_features, test_labels, batch_size=32)

model.save('./model')

print("Done!")
print("Loss: %.4f, accuracy: %.4f" % (loss, acc))
